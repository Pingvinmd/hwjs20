function filterCollection(collection, keywords, matchAll, ...fields) {
  // Розділяємо рядок з ключовими словами на окремі слова
  const keywordArray = keywords.trim().split(' ');

  // Фільтруємо колекцію
  return collection.filter(obj => {
    // Перевіряємо, чи містить об'єкт всі ключові слова, якщо потрібно
    if (matchAll) {
      return keywordArray.every(keyword =>
        // Перевіряємо, чи містить поле в об'єкті ключове слово
        fields.some(field => 
          // Дістаємо значення поля з об'єкта
          field.split('..').reduce((nestedObject, fieldName) =>
            nestedObject ? nestedObject[fieldName] : undefined,
            obj
          ).toString().toLowerCase().includes(keyword.toLowerCase())
        )
      );
    } else {
      return keywordArray.some(keyword =>
        fields.some(field =>
          field.split('..').reduce((nestedObject, fieldName) =>
            nestedObject ? nestedObject[fieldName] : undefined,
            obj
          ).toString().toLowerCase().includes(keyword.toLowerCase())
        )
      );
    }
  });
}
  
const collection = [
  { id: 1, name: 'John', age: 25, city: 'Kyiv' },
  { id: 2, name: 'Mary', age: 30, city: 'Lviv' },
  { id: 3, name: 'Bob', age: 35, city: 'Kyiv' },
  { id: 4, name: 'Alice', age: 27, city: 'Odessa' },
  { id: 5, name: 'Alex', age: 22, city: 'Kyiv' }
];
const filterrCollection = filterCollection(collection, 'Kyiv', false, 'name', 'city');
console.log(filterrCollection);

